#include <pcap.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/if_ether.h>
#include <netinet/ip6.h>
#include <netinet/tcp.h>

/*
struct ether_header
{
  uint8_t  ether_dhost[ETH_ALEN];
  uint8_t  ether_shost[ETH_ALEN];
  uint16_t ether_type;
} __attribute__ ((__packed__));

struct ip6_hdr {
    union {
        struct ip6_hdrctl {
	        uint32_t ip6_un1_flow;
	        uint16_t ip6_un1_plen;
	        uint8_t  ip6_un1_nxt;
	        uint8_t  ip6_un1_hlim;
	    } ip6_un1;
	    uint8_t ip6_un2_vfc;
      } ip6_ctlun;
    struct in6_addr ip6_src;
    struct in6_addr ip6_dst;
  };
*/

void printIPv6 (const uint8_t * addr) {
    for (int i = 0; i < 15; i++) {
        printf("%02x", addr[i]);
        if (i % 2)
            printf(":");
    }
    printf("%02x\n", addr[15]);
}

//swapping compare values to big endian by hand
uint32_t processHeader (const u_char * packet) {
    uint32_t packetLen = 0;
    //IPv6
    if (((struct ether_header *)packet)->ether_type != 0xdd86)
        return 0;

    packetLen += sizeof(struct ether_header);
    struct ip6_hdr * header = (struct ip6_hdr *)(packet + packetLen);

    //4 bits - version, expecting 6
    if ((header->ip6_ctlun.ip6_un1.ip6_un1_flow & 0x00f0) != 0x60)
        return 0;

    //8 bits - traffic class, expecting 0
    if (header->ip6_ctlun.ip6_un1.ip6_un1_flow & 0xf00f)
        return 0;

    packetLen += 40;    //default IPv6 packet length
    uint8_t next = header->ip6_ctlun.ip6_un1.ip6_un1_nxt;
    while (next != 6) {         //6 indicates TCP    
        switch (next) {
            case 0:
            case 43:
            case 51:
            case 60:
                next = *(packet + packetLen);
                packetLen += *(packet + packetLen + 1);
                break;
            case 44:
                next = *(packet + packetLen);
                packetLen += 4;
                break;
            default:
                return 0;

        }
    }

    //looking for well-known telnet port 23 == 0x17
    uint16_t * port = (uint16_t *)(packet + packetLen);
    if ((*port & 0xffff) != 0x1700 && (*(port + 1) & 0xffff) != 0x1700)
        return 0;

    return packetLen;
}

void process_packet (u_char * arg, const struct pcap_pkthdr * pkthdr, const u_char * packet) {
    uint32_t headersLen = processHeader(packet);
    if (headersLen == 0)
        return;

    struct tcphdr * tcp = (struct tcphdr *)(packet + headersLen);

    //checking if there is any payload
    if (tcp->doff * 4 + headersLen == pkthdr->len)
        return;
    headersLen += tcp->doff * 4;


    printf("%.*s\n", pkthdr->len - headersLen, packet + headersLen);
}

void process_packet_cinematic (u_char * arg, const struct pcap_pkthdr * pkthdr, const u_char * packet) {
    process_packet(arg, pkthdr, packet);
    system("sleep 0.01");
}

int main (int argc, char ** argv) {
	if (argc < 2) {
    	fprintf(stderr, "Expected format: %s <path_to_pcapfile>\n", *argv);
    	return 1;
	}

	if (strncmp(argv[1], "-h", 2) == 0 || strncmp(argv[1], "--help", 6) == 0) {
		printf("Parses a file with network traffic, picking out Telnet packets and shows their contents.\n");
		printf("Usage: %s [-c] <path_to_pcapfile>\n", argv[0]);
        printf("\t-c, --cinametic\n\t\tPrint contents slowly in order to be legible.\n");
		return 0;
	}

    int cinematic = (strncmp(argv[1], "-c", 2) == 0 || strncmp(argv[1], "--cinematic", 11) == 0);

	const char * file = argv[1 + cinematic];
	pcap_t * pcap = 0;
	char error_buffer [PCAP_ERRBUF_SIZE];
	memset(error_buffer, 0, PCAP_ERRBUF_SIZE);

	//open pcap file and check
	pcap = pcap_open_offline(file, error_buffer);
	if (! pcap) {
		fprintf(stderr, "ERROR: %s\n", error_buffer);
		return 1;
	}

	if (pcap_loop(pcap, -1, cinematic ? process_packet_cinematic : process_packet, 0) == -1) {
		fprintf(stderr, "ERROR: %s\n", pcap_geterr(pcap));
		return 1;
	}

	pcap_close(pcap);

	return 0;
}
