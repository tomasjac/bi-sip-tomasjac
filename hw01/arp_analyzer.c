#include <pcap.h>
#include <stdio.h>
#include <string.h>
#include <netinet/if_ether.h>

/*
struct arphdr {
    unsigned short int ar_hrd;
    unsigned short int ar_pro;
    unsigned char ar_hln;
    unsigned char ar_pln;
    unsigned short int ar_op;
};

struct	ether_arp {
	struct	arphdr ea_hdr;
	uint8_t arp_sha[ETH_ALEN];
	uint8_t arp_spa[4];
	uint8_t arp_tha[ETH_ALEN];
	uint8_t arp_tpa[4];
};

*/

int validProtocols (const struct arphdr * header) {
	//ethernet
	if (header->ar_hrd != 0x0100)
		return 0;

	//IPv4
	if (header->ar_pro != 0x0008)
		return 0;

	//address length, may be redundant to check
	if (header->ar_hln != 6 || header->ar_pln != 4)
		return 0;

	//valid operation
	if (header->ar_op != 0x0100 && header->ar_op != 0x0200)
		return 0;

	return 1;
}

int isBroadcast (const uint8_t * mac) {
	for (int i = 0; i < 6; i ++)
		if (mac[i] != 0 && mac[i] != 0xff)
			return 0;
	return 1;
}

//all compares of multiple bytes must take big endianess into consideration
void process_packet(u_char * arg, const struct pcap_pkthdr * pkthdr, const u_char * packet) {

	struct ether_arp * msg = (struct ether_arp *)(packet + 14);

	if (! validProtocols(&msg->ea_hdr))
		return;

	printf("Eth protocol - ");
	printf("src: ");
	for (int i = 0; i < 5; i ++)
		printf("%02x:", msg->arp_sha[i]);
	printf("%02x ", msg->arp_sha[5]);

	printf("dest: ");
	if (isBroadcast(msg->arp_tha))
		printf("broadcast ");
	else {
		for (int i = 0; i < 5; i ++)
			printf("%02x:", msg->arp_tha[i]);
		printf("%02x ", msg->arp_tha[5]);
	}

	printf("ARP: ");

	if (msg->ea_hdr.ar_op == 0x0100) {
		printf("Who has ");
		for (int i = 0; i < 3; i ++)
			printf("%d:", msg->arp_tpa[i]);
		printf("%d? Tell ", msg->arp_tpa[3]);
		for (int i = 0; i < 3; i ++)
			printf("%d:", msg->arp_spa[i]);
		printf("%d\n", msg->arp_spa[3]);
	}
	else {		//assuming ar_op == 0x0200, checked earlier
		for (int i = 0; i < 3; i ++)
			printf("%d:", msg->arp_spa[i]);
		printf("%d is at ", msg->arp_spa[3]);
		for (int i = 0; i < 5; i ++)
			printf("%02x:", msg->arp_sha[i]);
		printf("%02x\n", msg->arp_sha[5]);
	}
}

int main (int argc, char ** argv) {
	if (argc != 2) {
    	fprintf(stderr, "Expected format: %s <path_to_pcapfile>\n", *argv);
    	return 1;
	}

	if (strncmp(argv[1], "-h", 2) == 0 || strncmp(argv[1], "--help", 6) == 0) {
		printf("Parses a file with network traffic, picking out ARP packets and shows their contents.\n");
		printf("Usage: %s <path_to_pcapfile>\n", argv[0]);
		return 0;
	}

	const char * file = argv[1];
	pcap_t * pcap = 0;
	char error_buffer [PCAP_ERRBUF_SIZE];
	memset(error_buffer, 0, PCAP_ERRBUF_SIZE);

	//open pcap file and check
	pcap = pcap_open_offline(file, error_buffer);
	if (! pcap) {
		fprintf(stderr, "ERROR: %s\n", error_buffer);
		return 1;
	}

	if (pcap_loop(pcap, -1, process_packet, 0) == -1) {
		fprintf(stderr, "ERROR: %s\n", pcap_geterr(pcap));
		return 1;
	}

	pcap_close(pcap);

	return 0;
}
